using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.ApplicationModel.Resources.Core;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using System.Collections.Specialized;
using Windows.Storage;
using Windows.ApplicationModel;
using Windows.Data.Xml.Dom;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using Windows.Web.Syndication;


// 此文件定义的数据模型可充当在添加、移除或修改成员时
// 支持通知的强类型模型的代表性示例。所选
// 属性名称与标准项模板中的数据绑定一致。
//
// 应用程序可以使用此模型作为起始点并以它为基础构建，或完全放弃它并
// 替换为适合其需求的其他内容。

namespace News.Data
{    
    public sealed class ReadXml
    {        
        /**
         * 读入XML文件
         * 调用方式：XmlDocument doc = await readXml("Assets", "rss.xml");
         * @param xmlPath：xml文件路径
         * @param xmlFileName：文件名
         * @return XmlDocument
         * 
         */
        public static async Task<XmlDocument> readXml(string xmlPath, string xmlFileName)
        {
            StorageFolder storageFolder = await Package.Current.InstalledLocation.GetFolderAsync(xmlPath);

            StorageFile storageFile = await storageFolder.GetFileAsync(xmlFileName);

            XmlLoadSettings xmlloadsettings = new XmlLoadSettings();

            xmlloadsettings.ProhibitDtd = false;

            xmlloadsettings.ResolveExternals = false;

            xmlloadsettings.ElementContentWhiteSpace = true;

            XmlDocument doc = await XmlDocument.LoadFromFileAsync(storageFile, xmlloadsettings);
            return doc;
        }        
        public static async Task GetSiteData()
        {            
            XmlDocument doc = await readXml("Assets", "rss.xml");
            
            var nodelist = doc.SelectNodes("/root");
            var nodes = nodelist[0].SelectNodes("item");
            int index = 0;
            foreach (var node in nodes)
            {
                var childNodes = node.SelectNodes("body");
                childNodes = childNodes[0].SelectNodes("outline");
                
                
                foreach (var childNode in childNodes)
                {
                    string str = childNode.Attributes[1].InnerText;
                   
                }
                
                index++;
            }
        }
                   
    }
}
